import React from "react";

import { motion } from "framer-motion";
import "./Prices.css";
import { useLocation } from "react-router-dom";
import { Header } from "../Header/Header";
import Preview from "../Preview/Preview";
import Footer from "../Footer/Footer";

export const Prices = () => {
  const location = useLocation();
  const [isStatus, setIsStatus] = React.useState("all");

  const statusHandler = (data) => {
    setIsStatus(data);
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className="portfolio">
        <Header />
        <Preview title={"Цены"} class={"preview__prices"} />
        <section className="prices">
          <div className="prices__container">
            <h2>3000₽</h2>
            <div className="prices__wrapper">
              <p className="prices__text-content">Длительность:</p>
              <p className="prices__text-content">1 час</p>
            </div>
            <div className="prices__wrapper">
              <p className="prices__text-content">Количество фотографий:</p>
              <p className="prices__text-content">30-40</p>
            </div>
            <div className="prices__wrapper">
              <p className="prices__text-content">Консультация по стилю</p>
            </div>
            <span style={{ color: "#954141" }}>
              Аренда студии оплачивается отдельно
            </span>
          </div>
          <div className="prices__container">
            <h2>3000₽</h2>
            <div className="prices__wrapper">
              <p className="prices__text-content">Длительность:</p>
              <p className="prices__text-content">1 час</p>
            </div>
            <div className="prices__wrapper">
              <p className="prices__text-content">Количество фотографий:</p>
              <p className="prices__text-content">30-40</p>
            </div>
            <div className="prices__wrapper">
              <p className="prices__text-content">Консультация по стилю</p>
            </div>
            <span style={{ color: "#954141" }}>
              Аренда студии оплачивается отдельно
            </span>
          </div>
        </section>
        <Footer />
      </section>
    </motion.div>
  );
};
