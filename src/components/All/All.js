import React from "react";
import { motion } from "framer-motion";
import "./All.css";
import { all1, all2, all3 } from "../../utils/imageStorage";
import a6 from "../../images/all/6.jpg";
export default function All({ openImage }) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className="all">
        <div className="all__container">
          <div className="all__vertical">
            {all1.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-all"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
          <div className="all__horizontal">
            <div className="all__two">
              <div style={{ overflow: "hidden" }}>
                <img
                  className="all__image"
                  src={a6}
                  alt="photo-type-all"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            </div>
            {all2.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-all"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
          <div className="all__more">
            {all3.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-all"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
        </div>
      </section>
    </motion.div>
  );
}
