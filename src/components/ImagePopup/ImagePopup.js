import "./ImagePopup.css";
import { useEffect } from "react";

export default function ImagePopup({ image, openedImage, hideImage }) {
  const onKeydown = ({ key }: KeyboardEvent) => {
    switch (key) {
      case "Escape":
        hideImage();
        break;
    }
  };

  useEffect(() => {
    document.addEventListener("keydown", onKeydown);
    return () => document.removeEventListener("keydown", onKeydown);
  });

  return (
    <section
      className={openedImage ? "image-popup open" : "image-popup"}
      onClick={hideImage}
    >
      <div className="image-popup__container">
        <img
          className="image-popup__photo"
          src={image}
          alt="opened-image"
          onClick={(e) => e.stopPropagation()}
        />
      </div>
    </section>
  );
}
