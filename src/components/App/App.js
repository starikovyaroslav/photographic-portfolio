import React from "react";

import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import { Main } from "../Main/Main";
import { Portfolio } from "../Portfolio/Portfolio";
import { Prices } from "../Prices/Prices";
import { usePreloading } from "../../utils/usePreloading";

function App() {
  usePreloading();

  return (
    <div className="app">
      <Routes>
        <Route exact path="/" element={<Main />} />
        <Route exact path="/portfolio" element={<Portfolio />} />
        <Route exact path="/price" element={<Prices />} />
        <Route path="*" element={<Navigate to="/" replace />} />
      </Routes>
    </div>
  );
}

export default App;
