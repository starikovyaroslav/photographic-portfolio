import React from "react";
import { motion } from "framer-motion";
import "./Studio.css";
import { studio1, studio2, studio3 } from "../../utils/imageStorage";
export default function Studio({ openImage }) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className="studio">
        <div className="studio__container">
          {studio1.map((item, id) => (
            <div style={{ overflow: "hidden" }} key={id}>
              <img
                className="all__image"
                src={item}
                alt="photo-type-studio"
                onClick={openImage}
                loading={"lazy"}
              />
            </div>
          ))}
          <div className="studio__three">
            {studio2.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-studio"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
          {studio3.map((item, id) => (
            <div style={{ overflow: "hidden" }} key={id}>
              <img
                className="all__image"
                src={item}
                alt="photo-type-studio"
                onClick={openImage}
                loading={"lazy"}
              />
            </div>
          ))}
        </div>
      </section>
    </motion.div>
  );
}
