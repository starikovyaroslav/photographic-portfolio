import React, { useEffect } from "react";
import "./NavTab.css";

export default function NavTab({ onStatus }) {
  const [isStatus, setIsStatus] = React.useState(
    localStorage.getItem("nav-item")
  );

  const statusHandler = (evt) => {
    setIsStatus(evt.target.value);
    localStorage.setItem("nav-item", isStatus);
    onStatus(evt.target.value);
  };

  useEffect(() => {
    setIsStatus(localStorage.getItem("nav-item"));
  });

  return (
    <>
      <nav className="nav">
        <ul className="nav__items">
          <li id="all" className="nav__item" onClick={() => onStatus("all")}>
            Все
          </li>
          <li
            id="portraits"
            className="nav__item"
            onClick={() => onStatus("portraits")}
          >
            Портреты
          </li>
          <li
            id="studio"
            className="nav__item"
            onClick={() => onStatus("studio")}
          >
            Студийные
          </li>
          <li id="love" className="nav__item" onClick={() => onStatus("love")}>
            Love story
          </li>
        </ul>
      </nav>
      <select
        className="select"
        id="selectBox"
        value={isStatus}
        onChange={statusHandler}
      >
        <option className="select__item" value="all">
          Все
        </option>
        <option className="select__item" value="portraits">
          Портреты
        </option>
        <option className="select__item" value="studio">
          Студийные
        </option>
        <option className="select__item" value="love">
          Love story
        </option>
      </select>
    </>
  );
}
