import React from "react";
import { motion } from "framer-motion";
import "./Portraits.css";
import { portrait1, portrait2, portrait3 } from "../../utils/imageStorage";
export default function Portraits({ openImage }) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className="portraits">
        <div className="portraits__container">
          <div className="portraits__three">
            {portrait1.map((item, id) => (
              <div
                className={id === 0 ? "span" : ""}
                style={{ overflow: "hidden" }}
                key={id}
              >
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-portraits"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
          {portrait2.map((item, id) => (
            <div style={{ overflow: "hidden" }} key={id}>
              <img
                className="all__image"
                src={item}
                alt="photo-type-portraits"
                onClick={openImage}
                loading={"lazy"}
              />
            </div>
          ))}
          <div className="portraits__four">
            {portrait3.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-portraits"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
        </div>
      </section>
    </motion.div>
  );
}
