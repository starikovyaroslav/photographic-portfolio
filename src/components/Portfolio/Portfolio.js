import React from "react";
import { motion } from "framer-motion";
import "./Portfolio.css";
import { Header } from "../Header/Header";
import Preview from "../Preview/Preview";
import Footer from "../Footer/Footer";
import NavTab from "../NavTab/NavTab";
import All from "../All/All";
import Portraits from "../Portraits/Portraits";
import Studio from "../Studio/Studio";
import Love from "../Love/Love";
import ImagePopup from "../ImagePopup/ImagePopup";

export const Portfolio = () => {
  const [selectedImage, setSelectedImage] = React.useState("");
  const [isStatus, setIsStatus] = React.useState("all");
  const [openedImage, setOpenedImage] = React.useState(false);
  localStorage.setItem("nav-item", isStatus);

  const statusHandler = (data) => {
    setIsStatus(data);
  };

  const openImage = (evt) => {
    setSelectedImage(evt.target.src);
    setOpenedImage(true);
  };

  const hideImage = () => {
    setOpenedImage(false);
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className={openedImage ? "portfolio blur" : "portfolio"}>
        <Header openedImage={openedImage} />
        <Preview title={"Портфолио"} class={"preview__portfolio"} />
        <NavTab onStatus={statusHandler} />
        {isStatus === "all" && <All openImage={openImage} />}
        {isStatus === "portraits" && <Portraits openImage={openImage} />}
        {isStatus === "studio" && <Studio openImage={openImage} />}
        {isStatus === "love" && <Love openImage={openImage} />}
        <Footer />
      </section>
      <ImagePopup
        image={selectedImage}
        openedImage={openedImage}
        hideImage={hideImage}
      />
    </motion.div>
  );
};
