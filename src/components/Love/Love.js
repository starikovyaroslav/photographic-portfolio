import React from "react";
import { motion } from "framer-motion";
import "./Love.css";
import { love1, love2, love3 } from "../../utils/imageStorage";

export default function Love({ openImage }) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className="love">
        <div className="love__container">
          {love1.map((item, id) => (
            <div style={{ overflow: "hidden" }} key={id}>
              <img
                className="all__image"
                src={item}
                alt="photo-type-studio"
                onClick={openImage}
                loading={"lazy"}
              />
            </div>
          ))}
          <div className="love__three">
            {love2.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-studio"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
          <div className="love__last">
            {love3.map((item, id) => (
              <div style={{ overflow: "hidden" }} key={id}>
                <img
                  className="all__image"
                  src={item}
                  alt="photo-type-studio"
                  onClick={openImage}
                  loading={"lazy"}
                />
              </div>
            ))}
          </div>
        </div>
      </section>
    </motion.div>
  );
}
