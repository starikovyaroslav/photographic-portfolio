import React from "react";
import "./Main.css";
import { Header } from "../Header/Header";
import Preview from "../Preview/Preview";
import About from "../About/About";
import Footer from "../Footer/Footer";
import { motion } from "framer-motion";
export const Main = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.4 }}
    >
      <section className="main">
        <Header />
        <Preview title={"Олеся Шакина"} />
        <About />
        <Footer />
      </section>
    </motion.div>
  );
};
