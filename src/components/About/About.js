import React from "react";
import "./About.css";
import photo from "../../images/photo.jpg";

export default function About() {
  return (
    <section className="about">
      <div className="about__container">
        <div className="about__info">
          <p className="about__text">Привет!</p>
          <p className="about__text">
            Меня зовут Олеся, я фотограф в городе Новосибирск. Здесь можно
            ознакомиться с примерами моих работ и записаться на фотосессию
          </p>
        </div>
        <img src={photo} className="about__photo" alt="photographer" />
      </div>
    </section>
  );
}
