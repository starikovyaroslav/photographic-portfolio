import p1 from "../images/portraits/1.jpg";
import p2 from "../images/portraits/2.jpg";
import p3 from "../images/portraits/3.jpg";
import p4 from "../images/portraits/4.jpg";
import p5 from "../images/portraits/5.jpg";
import p6 from "../images/portraits/6.jpg";
import p7 from "../images/portraits/7.jpg";
import p8 from "../images/portraits/8.jpg";
import p9 from "../images/portraits/9.jpg";
import p10 from "../images/portraits/10.jpg";

import a1 from "../images/all/1.jpg";
import a2 from "../images/all/2.jpg";
import a3 from "../images/all/3.jpg";
import a5 from "../images/all/5.jpg";
import a7 from "../images/all/7.jpg";
import a8 from "../images/all/8.jpg";
import a10 from "../images/all/10.jpg";

import s1 from "../images/studio/1.jpg";
import s2 from "../images/studio/2.jpg";
import s3 from "../images/studio/3.jpg";
import s4 from "../images/studio/4.jpg";
import s5 from "../images/studio/5.jpg";
import s6 from "../images/studio/6.jpg";
import s7 from "../images/studio/7.jpg";
import s8 from "../images/studio/8.jpg";
import s9 from "../images/studio/9.jpg";

import l1 from "../images/love/1.jpg";
import l2 from "../images/love/2.jpg";
import l3 from "../images/love/3.jpg";
import l4 from "../images/love/4.jpg";
import l5 from "../images/love/5.jpg";
import l6 from "../images/love/6.jpg";
import l7 from "../images/love/7.jpg";
import l8 from "../images/love/8.jpg";
import l9 from "../images/love/9.jpg";

const love1 = [l1, l2, l3];
const love2 = [l4, l5, l6];
const love3 = [l7, l8, l9];

const studio1 = [s1, s2, s4];
const studio2 = [s3, s5, s6];
const studio3 = [s7, s8, s9];

const all1 = [a1, a8];
const all2 = [a2, a3];
const all3 = [a10, a7, a5];

const portrait1 = [p10, p1, p2];
const portrait2 = [p3, p4, p5];
const portrait3 = [p6, p7, p8, p9];

//Большой массив для предварительной загрузки
export const imageStorage = [
  ...all1,
  ...all2,
  ...all3,
  ...portrait1,
  ...portrait2,
  ...portrait3,
  ...studio1,
  ...studio2,
  ...studio3,
  ...love1,
  ...love2,
  ...love3,
];

export {
  all1,
  all2,
  all3,
  portrait1,
  portrait2,
  portrait3,
  studio1,
  studio2,
  studio3,
  love1,
  love2,
  love3,
};
