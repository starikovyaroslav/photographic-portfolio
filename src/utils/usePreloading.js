import { imageStorage } from "./imageStorage";
import a6 from "../images/all/6.jpg";
import { useLayoutEffect, useMemo } from "react";

//Здесь реализуется предварительная загрузка изображений
export const usePreloading = () => {
  useLayoutEffect(() => {
    [...imageStorage, a6].map((image) => {
      const newImage = new Image();
      newImage.src = image;
      window[image] = newImage;
    });
  }, [imageStorage, a6]);
};
